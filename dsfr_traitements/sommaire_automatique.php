<?php
/**
 * Fonction de traitement
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Traitement qui ajoute automatiquement un sommaire en fonction des intertitres présents dans le code HTML
 * 
 * Cette fonction est automatiquement appelée lors de l'utilisation de la balise SPIP `#TEXTE`
 * avec le pipeline `declarer_tables_interfaces`
 *
 * @param string $html
 *     Code HTML
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 **/
function traitement_dsfr_sommaire_automatique($html, $env = []) {

	// pas de code HTML
	if ( !$html ) {
		return $html;
	}

	/*
		Déclencher l'insertion du sommaire automatique ?
	*/
	defined('_DSFR_SOMMAIRE_AUTOMATIQUE_TOUJOURS_INSERER_PAR_DEFAUT') || define('_DSFR_SOMMAIRE_AUTOMATIQUE_TOUJOURS_INSERER_PAR_DEFAUT', false);
	if (
		// toujours insérer par défaut ?
		_DSFR_SOMMAIRE_AUTOMATIQUE_TOUJOURS_INSERER_PAR_DEFAUT ||
		// il y a la présence de `DSFR:inserer_sommaire_automatique` pour forcer l'insertion du sommaire
		strpos($html, '<!-- DSFR:inserer_sommaire_automatique') !== false
	) {
		$html = traitement_dsfr_sommaire_automatique_inserer($html, $env);
	}

	return $html;
}

/**
 * Ajoute un sommaire automatique en fonction des intertitres présents dans le code HTML
 *
 * @param string $html
 *     Code HTML
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 **/
function traitement_dsfr_sommaire_automatique_inserer($html, $env = []) {

	// pas de code HTML ou sommaire déjà présent
	if ( !$html || stripos($html, 'fr-summary') !== false ) {
		return $html;
	}

	// profondeur maximum du sommaire
	defined('_DSFR_SOMMAIRE_AUTOMATIQUE_PROFONDEUR_MAX_PAR_DEFAUT') || define('_DSFR_SOMMAIRE_AUTOMATIQUE_PROFONDEUR_MAX_PAR_DEFAUT', 2);
	$sommaire_profondeur_max = _DSFR_SOMMAIRE_AUTOMATIQUE_PROFONDEUR_MAX_PAR_DEFAUT;

	// nombre d'intertitres au minimum pour afficher le sommaire
	defined('_DSFR_SOMMAIRE_AUTOMATIQUE_NOMBRE_INTERTITRES_MIN_PAR_DEFAUT') || define('_DSFR_SOMMAIRE_AUTOMATIQUE_NOMBRE_INTERTITRES_MIN_PAR_DEFAUT', 2);
	$sommaire_nombre_intertitres_min = _DSFR_SOMMAIRE_AUTOMATIQUE_NOMBRE_INTERTITRES_MIN_PAR_DEFAUT;

	// titre du sommaire
	$sommaire_titre = '';

	// position du marqueur si il existe
	$position_marqueur_inserer_sommaire_automatique = strpos($html, '<!-- DSFR:inserer_sommaire_automatique');
	if ( $position_marqueur_inserer_sommaire_automatique !== false ) {
		$parametres_marqueur_inserer_sommaire_automatique = [];

		// prise en compte des paramètres du marqueur
		if ( preg_match('/<!-- DSFR:inserer_sommaire_automatique(.*)-->/', $html, $parametres) ) {
			$parametres = trim($parametres[1]);
			if ( !empty($parametres) ) {
				$parametres = unserialize(base64_decode($parametres));
				if ( is_array($parametres) ) {
					$parametres_marqueur_inserer_sommaire_automatique = $parametres;
					// profondeur maximum du sommaire
					if ( array_key_exists('profondeur_max',$parametres_marqueur_inserer_sommaire_automatique) ) {
						$sommaire_profondeur_max = intval($parametres_marqueur_inserer_sommaire_automatique['profondeur_max']);
					}
					// nombre d'intertitres au minimum pour afficher le sommaire
					if ( array_key_exists('nombre_intertitres_min',$parametres_marqueur_inserer_sommaire_automatique) ) {
						$sommaire_nombre_intertitres_min = intval($parametres_marqueur_inserer_sommaire_automatique['nombre_intertitres_min']);
					}
					// titre du sommaire
					if ( array_key_exists('titre',$parametres_marqueur_inserer_sommaire_automatique) ) {
						$sommaire_titre = trim($parametres_marqueur_inserer_sommaire_automatique['titre']);
					}
				}
			}
			// supprime le marqueur (ne change pas la position d'insertion)
			$html = preg_replace('/<!-- DSFR:inserer_sommaire_automatique(.*)-->/', '', $html);
		}

		// insertion des ancres (prise en compte uniquement à partir du marqueur)
		$html_avec_ancres = dsfr_traitement('ancre_intertitres', substr($html, $position_marqueur_inserer_sommaire_automatique), $env);
	}
	else {
		// insertion des ancres
		$html_avec_ancres = dsfr_traitement('ancre_intertitres', $html, $env);
	}

	$intertitres_avec_ancre = [];
	if ( preg_match_all('/<h([123456])([^>]*)>(.*)<\/h\\1>/UimsS', $html_avec_ancres, $intertitres_trouves, PREG_SET_ORDER) ) {
		/*
			- index 0 : la capture de l'intertitre complet
			- index 1 : le niveau de l'intertire (1,2,3,4,5 ou 6)
			- index 2 : attributs HTML de l'intertire
			- index 3 : le contenu du texte compris entre les balises de l'intertitre
		*/
		//var_dump($intertitres_trouves);

		foreach ( $intertitres_trouves as $intertitre ) {
			// prendre en compte uniquement les intertitres avec une ancre
			if ( strpos($intertitre[3], 'dsfr_ancre_intertitre') !== false ) {
				$intertitres_avec_ancre[] = $intertitre;
			}
		}

		// libère un peu de mémoire
		unset($intertitres_trouves);
	}

	// pas d'intertitre avec une ancre
	if ( empty($intertitres_avec_ancre) ) {
		return $html; // on ne touche pas au contenu du code HTML
	}

	// trouver le niveau minimum des intertitres qui consitue le niveau 1 du sommaire
	$intertitres_niveau_min = 6;
	foreach ( $intertitres_avec_ancre as $intertitre ) {
		$intertitres_niveau_min = min($intertitres_niveau_min, intval($intertitre[1]));
		if ( $intertitres_niveau_min == 1 ) {
			break;
		}
	}

	// trouver les intertitres de bon niveau
	$intertitres_de_bon_niveau = [];
	foreach ( $intertitres_avec_ancre as $intertitre ) {
		$niveau_intertitre = intval($intertitre[1]);
		if (
			!$sommaire_profondeur_max ||
			( $niveau_intertitre < ($sommaire_profondeur_max + $intertitres_niveau_min) )
		) {
			$intertitres_de_bon_niveau[] = $intertitre;
		}
	}

	// pas d'intertitre de bon niveau
	if ( empty($intertitres_de_bon_niveau) ) {
		return $html; // on ne touche pas au contenu du code HTML
	}

	// ancres du sommaire
	$sommaire_ancres = [];
	foreach ( $intertitres_de_bon_niveau as $intertitre ) {
		if ( preg_match('/(.*)<a class="dsfr_ancre_intertitre.*id="([^"]*)"/', $intertitre[3], $intertitre_avec_ancre_trouve) ) {
			$id_ancre = isset($intertitre_avec_ancre_trouve[2]) ? $intertitre_avec_ancre_trouve[2] : false;
			$texte_intertitre = isset($intertitre_avec_ancre_trouve[1]) ? trim($intertitre_avec_ancre_trouve[1]) : false;

			if ( $id_ancre && $texte_intertitre ) {
				$sommaire_ancres[] = [
					'profondeur'	=> intval($intertitre[1]) - $intertitres_niveau_min + 1,
					'url'			=> '#'.$id_ancre,
					'texte'			=> $texte_intertitre,
				];
			}
		}
	}

	// pas d'ancre pour le sommaire ?
	if ( empty($sommaire_ancres) ) {
		return $html; // on ne touche pas au contenu du code HTML
	}

	// il n'y a pas suffisamment d'intertitres pour afficher le sommaire
	if ( $sommaire_nombre_intertitres_min > count($sommaire_ancres) ) {
		return $html; // on ne touche pas au contenu du code HTML
	}

	// prépare une arborescence propre (si jamais des niveaux on été sauté)
	$sommaire_arborescence_propre = [];
	$sommaire_arborescence_propre_profondeur = 1; // profondeur la plus bassse trouvé
	$profondeur = 0;
	foreach ( $sommaire_ancres as $ancre ) {

		// descendre dans l'arborescence
		while ( $ancre['profondeur'] > $profondeur ) {
			$profondeur++;
			if ( $ancre['profondeur'] != $profondeur ) {
				$sommaire_arborescence_propre[] = [
					'profondeur' => $profondeur,
				];
			}
		}

		if ( $profondeur > $sommaire_arborescence_propre_profondeur ) {
			$sommaire_arborescence_propre_profondeur = $profondeur;
		}

		// remonter dans l'arborescence
		while ( $ancre['profondeur'] < $profondeur ) {
			$profondeur--;
			if ( $ancre['profondeur'] != $profondeur ) {
				$sommaire_arborescence_propre[] = [
					'profondeur' => $profondeur,
				];
			}
		}

		$sommaire_arborescence_propre[] = $ancre;
	}

	$sommaire_arborescence = $sommaire_arborescence_propre;
	for ( $profondeur = $sommaire_arborescence_propre_profondeur; $profondeur > 1; $profondeur-- ) {
		$cle_precedente = 0;
		foreach ( $sommaire_arborescence as $cle => $valeur ) {
			if ( $valeur['profondeur'] == $profondeur ) {

				// ne pas afficher quand on remonte dans l'arborescence sans aucun contenu
				if ( !empty($valeur['url']) || !empty($valeur['texte']) || !empty($valeur['arborescence']) ) {
					$cle_parent = $cle_precedente;
					if ( !isset($sommaire_arborescence[$cle_parent]['arborescence']) ) {
						$sommaire_arborescence[$cle_parent]['arborescence'] = [];
					}
					$sommaire_arborescence[$cle_parent]['arborescence'][] = $valeur;
				}
				
				unset($sommaire_arborescence[$cle]);
			}
			else {
				$cle_precedente = $cle;
			}
		}
	}

	$sommaire_automatique = recuperer_fond('dsfr_composants/sommaire',[
		'titre'				=> $sommaire_titre,
		'arborescence'		=> $sommaire_arborescence,
		'attribut_class'	=> 'fr-my-3w fr-my-md-6w', // ajoute automatiquement un espace
	]);

	// bug ?
	if ( empty($sommaire_automatique) ) {
		return $html; // on ne touche pas au contenu du code HTML
	}

	// il y a un marqueur d'insertion
	if ( $position_marqueur_inserer_sommaire_automatique !== false ) {
		return substr($html, 0, $position_marqueur_inserer_sommaire_automatique).$sommaire_automatique.$html_avec_ancres;
	}
	
	// ajoute le sommaire au début
	return $sommaire_automatique.$html_avec_ancres;
}
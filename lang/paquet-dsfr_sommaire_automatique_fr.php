<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dsfr_sommaire_automatique_description' => '
Le plugin {{DSFR Sommaire Automatique}} vous permet de générer automatiquement un sommaire spécifique au DSFR à partir des intertitres de vos contenus éditoriaux.
',
	'dsfr_sommaire_automatique_nom' => 'DSFR Sommaire Automatique',
	'dsfr_sommaire_automatique_slogan' => 'Sommaire Automatique pour le Système de Design de l\'État',
);
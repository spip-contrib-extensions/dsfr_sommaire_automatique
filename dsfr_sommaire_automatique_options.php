<?php
/**
 * Options du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/******************************************************************************************
 * CONFIGURATIONS SPIP
 * 
 * Variables et constantes de personnalisation dédiées à SPIP
 * Voir : https://www.spip.net/fr_rubrique643.html
 **/

// ajoute le marqueur du plugin
$GLOBALS['marqueur_skel'] = ($GLOBALS['marqueur_skel'] ?? '').':dsfr_sommaire_automatique';
<?php
/**
 * Pipelines SPIP utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Déclarer les interfaces des tables SPIP
 * 
 * Permet de définir des traitements automatiques sur certaines
 * balises SPIP. L'étoile (#BALISE*) désactive ces traitements.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interface
 * 		Déclarations d'interface pour le compilateur
 * @return array $interface
 * 		Déclarations d'interface complétée pour le compilateur
 **/
function dsfr_sommaire_automatique_declarer_tables_interfaces($interface) {

	// active la gestion du sommaire automatique en fonction des intertitres sur toutes les balises SPIP `#TEXTE`
	defined('_DSFR_TRAITEMENT_AUTOMATIQUE_SOMMAIRE_AUTOMATIQUE') || define('_DSFR_TRAITEMENT_AUTOMATIQUE_SOMMAIRE_AUTOMATIQUE', true);
	if ( _DSFR_TRAITEMENT_AUTOMATIQUE_SOMMAIRE_AUTOMATIQUE ) {
		if ( !defined('_TRAITEMENT_DSFR_SOMMAIRE_AUTOMATIQUE') ) {
			define('_TRAITEMENT_DSFR_SOMMAIRE_AUTOMATIQUE', 'dsfr_traitement(\'sommaire_automatique\', %s, $Pile[0])');
		}
		foreach ( $interface['table_des_traitements']['TEXTE'] as $cle => $valeur ) {
			$interface['table_des_traitements']['TEXTE'][$cle] = str_replace('%s', $valeur, _TRAITEMENT_DSFR_SOMMAIRE_AUTOMATIQUE);
		}
	}

	return $interface;
}
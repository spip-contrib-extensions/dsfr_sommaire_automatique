# Plugin SPIP : DSFR Sommaire Automatique

Ce plugin repère les intertitres de vos contenus éditoriaux et s'en sert pour génèrer automatiquement un sommaire
spécifique au DSFR.

Ce plugin est maintenu et géré par les équipes de l'[Académie d'Orléans-Tours](https://spip.ac-orleans-tours.fr).


## 🇫🇷 Système de Design de l'État

[DSFR](https://www.systeme-de-design.gouv.fr) Le Système de Design de l'État (ci-après, le **DSFR**) est un ensemble
de composants web HTML, CSS et Javascript pour faciliter le travail des équipes projets des sites Internet publics,
et créer des interfaces numériques de qualité et accessibles.

Son utilisation par les administrations est soumise à une demande d'agrément.

[Voir la documentation officielle](https://www.systeme-de-design.gouv.fr).

### Licence et droit d'utilisation

##### ⚠️ Utilisation interdite en dehors des sites Internet de l'État

>Il est formellement interdit à tout autre acteur d'utiliser le Système de Design de l'État (les administrations
territoriales ou tout autre acteur privé) pour des sites web ou des applications. Le Système de Design de l'État
représente l'identité numérique de l'État. En cas d'usage à des fins trompeuses ou frauduleuses, l'État se réserve
le droit d'entreprendre les actions nécessaires pour y mettre un terme.

Voir les [conditions générales d'utilisation](https://github.com/GouvernementFR/dsfr/blob/main/doc/legal/cgu.md).

##### ⚠️ Prohibited Use Outside Government Websites

>This Design System is only meant to be used by official French public services' websites and apps. Its main purpose
is to make it easy to identify governmental websites for citizens. See terms.


## Compatibilité et prérequis du plugin SPIP

Le plugin est compatible avec :

 * [SPIP 4.2+](https://www.spip.net)
 * [PHP 8.1+](https://www.php.net)

Il nécessite préalablement l'installation du plugin SPIP [DSFR Composants](https://contrib.spip.net/DSFR-Composants)
# Changelog : DSFR Sommaire Automatique

Changelog du plugin SPIP **DSFR Sommaire Automatique**. Pour connaitre les changements concernant le DSFR vous
pouvez consulter son changelog ici : https://github.com/GouvernementFR/dsfr/blob/main/CHANGELOG.md


## 0.3.1 - 2024-10-04

### Fixed

- Espacement du sommaire

### Added

- Possibilité de définir un titre pour le sommaire


## 0.3.0 - 2024-07-19

### Fixed

- Plugin toujours en `dev` et non stable (fix du changement malvenu de @JamesRezo)


## 0.2.0 - 2024-07-05

### Added

- Compatibilité SPIP 4.*

### Changed

- Éviter les confusions `attribut_id`, `attribut_class`


## 0.1.0 - 2024-06-03

- Version initiale du plugin
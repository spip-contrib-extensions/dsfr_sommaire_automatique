<?php
/**
 * Fonctions de raccourci typographique DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Configuration et description du raccourci typographique.
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_sommaire_automatique() {
	return [
		'nom'			=> 'Sommaire automatique - Summary',
		'porte_plume'	=> [
			'openWith'		=> "\n<dsfr-sommaire_automatique>\n",
		],
	];
}

/**
 * Traitement du raccourci typographique.
 * 
 * @param array $parametres
 * @param string $texte
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_sommaire_automatique_traitement($parametres = [], $texte = '') {

	// gestion des erreurs
	$erreurs = [];
	if ( empty(!$erreurs) ) {
		return ['erreur' => $erreurs];
	}

	// supprimer les paramètres invalides pour ne pas les transmettre au squelette
	$parametres = array_intersect_key($parametres, array_flip(['profondeur_max','nombre_intertitres_min','titre']));

	// retour du traitement
	// ajoute les paramètres dans le commentaire qui sera traité après `propre()` par `traitement_dsfr_sommaire_automatique_inserer`
	return ['raccourci' => '<!-- DSFR:inserer_sommaire_automatique'.(!empty($parametres) ? ' '.base64_encode(serialize($parametres)) : '').' -->'];
}